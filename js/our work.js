let loadImg = document.querySelectorAll('.work_img_properties');
let buttonAddImg = document.querySelector('.work_load');
let buttonAll = document.querySelector('.work_all_select');
let firstImgs = document.querySelectorAll('.first_imgs');
buttonAddImg.addEventListener('click', loadNewImg);
function loadNewImg(){
    loadImg.forEach( item => {
        item.classList.add('img_active');
    });
    buttonAddImg.classList.add('button_none');
}
let selectImg = function(){
    let tabNav = document.querySelectorAll('.our_work_select'),
    tabCont = document.querySelectorAll('.work_img_properties'),
    tabName;
    
tabNav.forEach(item => {
    item.addEventListener('click', selectTabNav)
    
})
function selectTabNav(){
    tabNav.forEach( item => {
        item.classList.remove('tab_active');
    });
    this.classList.add('tab_active');
    tabName = this.getAttribute('data-tab-name');
    selectTabContent(tabName);

}
function selectTabContent(tabName){
    tabCont.forEach(item => {
        item.classList.contains(tabName)? item.classList.add('img_active'): item.classList.remove('img_active');
           });

    }  
    buttonAll.addEventListener('click', function(){
        if(buttonAddImg.classList.contains('button_none')){
            tabCont.forEach( item => {
                item.classList.add('img_active');
            })
        }
        else{
            firstImgs.forEach(item => {
                item.classList.add('img_active');
            })
        }
    }) 
};
selectImg();



